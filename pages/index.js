import React from 'react'
import Head from 'next/head'
import styles from '../styles/Home.module.css'
import Banner from '../components/Banner';
import Highlights from '../components/Highlights'

export default function Home() {
	
	const data= {
		title: "Budget Tracker",
		content: "Tracks for everyone, everywhere",
		destination: "/transactions",
		label: "Track Now!"			
		}
return (
		<React.Fragment>
		
		<Banner data={data}/>
		<Highlights/>
		</React.Fragment>
	)
}
