
import React, {useState, useEffect} from 'react';
import Router from 'next/router';
import {Form, Button, Container} from 'react-bootstrap';
import Head from 'next/head';
import AppHelper from '../../app-helper';
export default function index() {
	const [email, setEmail] = useState("")
	const [firstName, setFirstName] = useState("")
	const [lastName, setLastName] = useState("")
	const [password1, setPassword1] = useState("")
	const [password2, setPassword2] = useState("")
	const [isActive, setIsActive] = useState(false)
	
	useEffect(() => {
		//validation to enable submit button when all fields are populated and passwords match
		if((email !== "" && password1 !== "" && password2 !== "") && (password1 === password2)) {
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [firstName, lastName, email, password1, password2])

	function registerUser(e) {
		e.preventDefault()
		setFirstName("")
		setLastName("")		
		setEmail("")
		setPassword1("")
		setPassword2("")

	//validation to enable submit button when all fields are populated and both passwords match
	//mobile  number is = 11
	// if(password1 === password2 || mobileNumber.toString().length === 11){
	// 	console.log(1)
	// 	e.preventDefault()
	// }
	//check the database
	//check for duplicate email in database first
	//url- where we can get the duplicate routes in our server
	//asynchronously load contents of the URL
		const options = {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				"email": email
			})
		}
		fetch(`${ AppHelper.API_URL }/users/email-exists`, options)
		//return a promise that resolves when the result is loaded
		.then(AppHelper.toJSON)//call this junction when the result is loaded
		.then(data => {
			//if no duplicates found
			//get the route for registration in our server
			if(data === true) {
			alert('Email already used')
			}
			else if (data === false) {
				const options =  {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						"firstName": firstName,
						"lastName": lastName,
						"email": email,
						"password": password1,
					})
				}

				fetch(`${ AppHelper.API_URL }/users`, options)
				.then(AppHelper.toJSON)
				.then(data => {
					console.log(data)
					//if registration is successful
					if(data === true){
						alert("Registered Successfully")
						//redirect to login page
						Router.push('/login')
					}else{
						//error occured in registration
						alert("Something Went wrong")
					}

				})
			}


		})


		// console.log(`Registration successful. ${email} registered with password ${password1}`)
	}


	return (
		<React.Fragment>
		<Head>
			<title>Register</title>
		</Head>
		<Container>
		<Form className="my-3" onSubmit={e => registerUser(e)}>
			<Form.Group controlId="firstName">
				<Form.Label>First Name:</Form.Label>
				<Form.Control type="text" placeholder="Enter your First Name" value={firstName} onChange={e => setFirstName(e.target.value)} required/>
			</Form.Group>

			<Form.Group controlId="lastName">
				<Form.Label>Last Name:</Form.Label>
				<Form.Control type="text" placeholder="Enter your Last Name" value={lastName} onChange={e => setLastName(e.target.value)} required/>
			</Form.Group>

			<Form.Group controlId="email">
				<Form.Label>Email:</Form.Label>
				<Form.Control type="email" placeholder="Enter your email" value={email} onChange={e => setEmail(e.target.value)} required/>
				<Form.Text className="text-muted">We'll never share your email with anyone else.</Form.Text>
			</Form.Group>
			
			<Form.Group controlId="password1">
				<Form.Label>Password:</Form.Label>
				<Form.Control type="password" placeholder="Enter your password" value={password1} onChange={e => setPassword1(e.target.value)} required/>
				
			</Form.Group>
			
			<Form.Group controlId="password2">
				<Form.Label>Verify your password</Form.Label>
				<Form.Control type="password" placeholder="Type password again" value={password2} onChange={e => setPassword2(e.target.value)} required/>
			</Form.Group>
			{isActive
				? <Button variant="primary" type="submit" id="sumbitBtn">Submit</Button>
				: <Button variant="primary" type="submit" id="sumbitBtn" disabled>Submit</Button>
			}
		</Form>
		</Container>
		</React.Fragment>
	)
}