import React, { useState, useEffect } from 'react';
import NavBar from '../components/NavBar';
import { Container } from 'react-bootstrap';
import '../styles/globals.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import {UserProvider} from '../UserContext.js';
import AppHelper from '../app-helper';


function MyApp({ Component, pageProps }) {
   //user state is an object with properties from our local storage
	const [user, setUser] = useState({
		id: null,
		data:null
	})

	//function to clear local storage upon logout
	const unsetUser = () => {
		localStorage.clear();

	setUser({
	  id: null,
	  data:null
		})
	}

	useEffect(()=>{
		const options = {
			headers: { Authorization: `Bearer ${ AppHelper.getAccessToken()}`}
		}
		fetch(` ${AppHelper.API_URL }/users/details`, options)
			.then(AppHelper.toJSON)
			.then(data => {
				if (typeof data._id !== undefined) {
					setUser({ 
						
						data: data,
						id: data._id
					})
				}else {
					setUser({ id: null,
					data:null
					})
				}
			})

	}, [user.id])

	useEffect(() => {
        // console.log(`User with id: ${user.id}`);
    }, [ user.id ]);	

	return (
		<React.Fragment>
			<UserProvider value={{ user, setUser, unsetUser}}>
		  	<NavBar/>
		  	<Container fluid className="">
		  		<Component {...pageProps} />
		  	</Container>
			</UserProvider>
		</React.Fragment>

	)
}

export default MyApp
