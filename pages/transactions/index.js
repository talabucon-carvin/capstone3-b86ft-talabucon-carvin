import React, { useContext } from 'react';
import Head from 'next/head';
import Transaction from '../../components/Transaction';
import CreateTransaction from '../../components/CreateTransaction'
// import styles from '../../styles/Transactions.module.css'
import UserContext from '../../UserContext'
import{ Container, Button , Table, Row, Col } from 'react-bootstrap';
import styles from '../../styles/transactions.module.css'

export default function index(data) {
const { user } = useContext(UserContext)
// console.log(user)
// console.log(data)
	return (
		<React.Fragment>
			
			<Row>
				<Col md={3} style={{background: '#DCC7AA' }} className="pt-2">
					<CreateTransaction/>
				</Col>
				<Col md={9} style={{background: '#e0d5c5' }}>
					<Transaction/>
				</Col>
			</Row>
			
		</React.Fragment>
	)
}

