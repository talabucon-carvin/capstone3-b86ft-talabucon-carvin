import React, { useContext } from 'react';
import DoughnutChart from '../../components/DoughnutChart'
import UserContext from '../../UserContext';

export default function index(){
const { user } = useContext(UserContext)
	return (

	<React.Fragment>
		<DoughnutChart/>
	</React.Fragment>


	)

}