import React, {useState, useEffect, useContext} from 'react'
import {Container, Form, Button, Row, Col} from 'react-bootstrap';
import AppHelper from '../app-helper';
import {Doughnut, Line, Pie} from 'react-chartjs-2'

import UserContext from '../UserContext'
export default function DoughnutChart () {
	const { user } = useContext(UserContext)
	const [actions, setActions] = useState([])
	
	const options = {
	method:"POST",
	headers: { 'Content-Type': 'application/json' },
	body: JSON.stringify({
		userID: user.id
		})
	}
	
	useEffect(() => {
		const data = localStorage.getItem("storeT");
		
		if(data) {
		setActions(JSON.parse(data))
		}
	},[])

	useEffect( () => {
		fetch(`${ AppHelper.API_URL }/transactions/history`, options)
		.then(res => res.json())
		.then (actions => {
			// console.log(actions)
			// setActions(actions)
			localStorage.setItem("storeT", JSON.stringify(actions))
		})
	},[])

	const filteredIncome = actions.filter(action => action.type == "income")
	const income = filteredIncome.reduce((total, currentValue) => total = total + currentValue.amount,0);

	//calculate expense
	const filteredExpense = actions.filter(action => action.type == "expense")
	const expenses = filteredExpense.reduce((total, currentValue) => total = total + currentValue.amount,0);

	//calculate balance
	const balance = income - expenses
	const dateData = [] 
	const incomeData = []
	const incomecategoryData = []
	filteredIncome.map(data => {
		incomeData.push(data.amount)
		incomecategoryData.push(data.category)
	})
	
	const expensecategoryData = []
	const expenseData = []

	filteredExpense.map(data => {
		expenseData.push(data.amount)
		expensecategoryData.push(data.category)
	})
	
	return(
		<React.Fragment>
			<Container> 
				<Row>
					<Col md={6}>
						<h4>Income to Expense Ratio</h4>
						<Pie
							data={{
								datasets:[{
									data:[income, expenses],
									backgroundColor:["yellow", "red"]
								}],
								labels: ["income", "expenses"]
							}}
							redraw={false}
						/>
					</Col>
					<Col md={6}>
						<h4>Income</h4>
						<Pie
							data={{
								datasets:[{
									data:incomeData,
									backgroundColor:["yellow", "red"]
								}],
								labels: incomecategoryData
							}}
							redraw={false}
						/>
					</Col>		
				</Row>
				<Row>
					<Col md={6}>
						<h4>Expenses</h4>
						<Pie
							data={{
								datasets:[{
									data:expenseData,
									backgroundColor:["yellow", "red"]
								}],
								labels: expensecategoryData
							}}
							redraw={false}
						/>
					</Col>					
					<Col md={6}>
						<h4>Monthly Balance Trend</h4>
						<Line
							data={{
								datasets:[{
									data:[balance],
									backgroundColor:["yellow", "red"]
								}],
								labels: [balance]
							}}
							options={{
								title:{
								display:true,
								text:'Monthly Trend',
								fontSize:20
								},
								legend:{
								display:true,
								position:'right'
								}
							}}

						/>
					</Col>
				</Row>
			</Container>
		</React.Fragment>





	)
}