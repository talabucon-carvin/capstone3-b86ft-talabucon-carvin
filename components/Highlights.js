import{ Row, Col, Card } from 'react-bootstrap';

export default function Highlights() {
 return (
 	<Row>
 		<Col xs={12} md={4}>
 			<Card className="cardHighlight">
 				<Card.Body>
 					<Card.Title>
 						<h2>Keep Track of your Budget</h2>
 					</Card.Title>
 					<Card.Text>
 					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec in finibus mauris. Fusce condimentum tempor nisi non faucibus. Vivamus tristique turpis laoreet fringilla ornare. Integer at felis bibendum, tristique leo eget, vestibulum tellus. Quisque neque felis, fringilla et elementum mollis, malesuada et tellus. Integer finibus rutrum nunc, sed accumsan metus pretium quis. Nullam et dictum dolor. Nam sodales neque sem, elementum placerat lectus luctus non. Sed placerat leo quis tincidunt efficitur.

 					</Card.Text>
 				</Card.Body>
 			</Card>
 		</Col>
 		<Col xs={12} md={4}>
 			<Card className="cardHighlight">
 				<Card.Body>
 					<Card.Title>
 						<h2>Spend wisely</h2>
 					</Card.Title>
 					<Card.Text>
 					Cras nec ligula feugiat, finibus est nec, viverra leo. Fusce ac libero id metus auctor scelerisque in in neque. Suspendisse in quam mattis ante venenatis hendrerit. Maecenas dui risus, placerat ut lectus id, volutpat pretium justo. Nunc luctus maximus tempor. Fusce aliquet nunc et quam elementum, vel tincidunt mi volutpat. Phasellus ac turpis ut nisl placerat hendrerit at sit amet lectus. Nunc ut dolor sed mi elementum pulvinar.

 					</Card.Text>
 				</Card.Body>
 			</Card>
 		</Col>
 		<Col xs={12} md={4}>
 			<Card className="cardHighlight">
 				<Card.Body>
 					<Card.Title>
 						<h2>See your dashboard</h2>
 					</Card.Title>
 					<Card.Text>
 					Ut a commodo purus. Sed tempor turpis a molestie volutpat. Quisque mauris orci, condimentum eu accumsan a, dapibus quis purus. Sed condimentum, neque molestie tincidunt hendrerit, urna dui dignissim est, vel iaculis tortor dui nec felis. Vivamus dictum condimentum eros nec porta. Donec accumsan justo ut est ultricies pretium. Pellentesque non egestas quam, eu convallis quam. Praesent cursus, sapien at tincidunt lacinia, nisi eros eleifend velit, non sollicitudin elit tellus in nisi. Fusce volutpat dui quis vehicula ullamcorper. Morbi semper rhoncus diam, quis volutpat eros sollicitudin vulputate. Etiam enim quam, lobortis eget elementum ut, convallis ut massa.
 					</Card.Text>
 				</Card.Body>
 			</Card>
 		</Col>
  	</Row>
 	)
}