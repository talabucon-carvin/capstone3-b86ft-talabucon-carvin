import React, {useContext} from 'react';
import { Navbar, Nav } from 'react-bootstrap';
import UserContext from '../UserContext'
import Link from 'next/link'

export default function NavBar() {
const { user } = useContext(UserContext)

	return(
		<Navbar bg="light" expand="lg">
		<Navbar.Brand href="/">Budget Tracker</Navbar.Brand>
		<Navbar.Toggle aria-controls="basic-navbar-nav"/>
		<Navbar.Collapse id="basic-navbar-nav">
			<Nav className="mr-auto">
				<React.Fragment>
					<Link href="/">
						<a className="nav-link" role="button">Home</a>
					</Link>

				</React.Fragment>
				{(user.id !== undefined)
					?
					<React.Fragment>
						<Link href="/transactions">
							<a className="nav-link" role="button">Transactions</a>
						</Link>
						<Link href="/dashboard">
							<a className="nav-link" role="button">Dashboard</a>
						</Link>
						<Link href="/logout">
							<a className="nav-link" role="button">Logout</a>
						</Link>
					</React.Fragment>
						
					: 
					<React.Fragment>
						<Link href="/login">
							<a className="nav-link" role="button">Log In</a></Link>
						<Link href="/register">
							<a className="nav-link" role="button">Register</a></Link>
					</React.Fragment>
				}
			</Nav>        
		</Navbar.Collapse>
		</Navbar>
	)
}

