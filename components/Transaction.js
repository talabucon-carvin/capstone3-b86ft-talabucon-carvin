import React, {useContext, useState, useEffect} from 'react';
import moment from 'moment'
import{ Col, Button, Card, ListGroup, Form, Modal, Table } from 'react-bootstrap';
// import PropTypes from 'prop-types';
import AppHelper from '../app-helper';
import Swal from 'sweetalert2';
import UserContext from '../UserContext'

export default function Transaction() {
	const [show, setShow] = useState(false);

	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);
	//userid
	const { user } = useContext(UserContext)
	const [search, setSearch] = useState("");
	const [render, setRender] = useState([])
	const [description, setDesc] = useState("")
	const [amount, setAmount] = useState("")
	const [type, setType] = useState("")
	const [category, setCategory] = useState("")
	const [type2, setType2] = useState("")
	const [category2, setCategory2] = useState("")
	const [id, setId] = useState("")
	//fetch history
	// console.log(user.id)
	const [actions, setActions] = useState([])
	const options = {
		method:"POST",
		headers: { 'Content-Type': 'application/json' },
		body: JSON.stringify({
			userID: user.id
		})
	}
	
	useEffect(() => {
		const data = localStorage.getItem("storeT");
		if(data) {
		setActions(JSON.parse(data))
		}
	},[])


	useEffect( () => {
		fetch(`${ AppHelper.API_URL }/transactions/history`, options)
		.then(res => res.json())
		.then (actions => {
			// console.log(actions)
			setActions(actions)
			localStorage.setItem("storeT", JSON.stringify(actions))
		})
		
	})	
    function createTransaction(e) {
    	e.preventDefault()
    	let searchactions = actions.filter(action => (action.type == search)||(action.category.toLowerCase() == search.toLowerCase())||(action.description.toLowerCase() == search.toLowerCase()))
    	transactionsData = searchactions.map(action => {
 			
 			return(
						<tr key={action._id}>
							<td>{action.description}</td>
							{
							(action.type == "income")
							?
							<React.Fragment>
								<td className="text-right">{action.amount}</td>
								<td></td>
							</React.Fragment>
							:
							<React.Fragment>
								<td></td>
								<td className="text-right">{action.amount}</td>
							</React.Fragment>
							}
							<td>{action.type}</td>
							<td>{action.category}</td>
							<td>{moment(action.createdOn).format('LL')}</td>
							<td>{moment(action.dateEarnedOrSpenT).format('MMMM')}</td>
						</tr>

			)
 	
 		
		})
    	setRender(transactionsData)
    }
// console.log(user)
// console.log(data)
	function editTransaction(e) {
		e.preventDefault()
		setId(e.target.value)

	// 	fetch(`${ AppHelper.API_URL }/transactions`, {
	// 		method: 'PUT',
	// 		headers: {
	// 			'Content-Type': 'application/json',
	// 		},
	// 		body: JSON.stringify({
	// 			"_id": action._id,
	// 			"description": description,
	// 			"amount": amount,
	// 			"type": type,
	// 			"category": category
	// 		})
	// 	})
	// 	.then(res => {
	// 		return res.json()
	// 	})
	// 	.then(data => {
	// })
	}
	function deleteTransaction(e) {
		e.preventDefault()
		console.log(e.target.value)
		setId(e.target.value)
		console.log(id)

			fetch(`${ AppHelper.API_URL }/transactions`, {
				method: 'DELETE',
				headers: {
					'Content-Type': 'application/json',
				},
				body: JSON.stringify({
					_id: e.target.value,
					userid: user.id
				})
			})
		.then(res => {
		console.log(res)
		return res.json()
		})
		.then (data=> {
			console.log(data)
		})
			
		
		// .then(data => {
		// 	console.log(data)
		// })
	}




	let transactionsData = actions.map(action => {
			
		return(

			<tr key={action._id}>
				<td>{action.description}</td>
				{
				(action.type == "income")
				?
				<React.Fragment>
					<td className="text-right">{action.amount}</td>
					<td></td>
				</React.Fragment>
				:
				<React.Fragment>
					<td></td>
					<td className="text-right">{action.amount}</td>
				</React.Fragment>
				}
				<td>{action.type}</td>
				<td>{action.category}</td>
				<td>{moment(action.createdOn).format('LL')}</td>
				<td>{moment(action.dateEarnedOrSpenT).format('MMMM')}</td>
				<td>
					<Button name="id" type="button" value={action._id} onClick={e => createTransaction(e)}>
							Edit
					</Button>

					
{/*					<Modal
						size="lg"
				        show={show}
				        onHide={handleClose}
				       
				    >
				    	<Modal.Header closeButton>
				    	    <Modal.Title id="example-modal-sizes-title-lg">
								Edit Transaction
							</Modal.Title>
				    	</Modal.Header>
						<Modal.Body>
							<Form className="my-3" onSubmit={e => editTransaction(e)}>
								<Form.Group controlId="id">
									<Form.Label>Id</Form.Label>
									<Form.Control type="text" placeholder={action._id} required>
									</Form.Control>
									
								</Form.Group>
								<Form.Group controlId="description">
									<Form.Label>Desctiption</Form.Label>
									<Form.Control type="textarea" value={description} onChange={e => setDescription(e.target.value)} required>
									</Form.Control>
									
								</Form.Group>
								<Form.Group controlId="amount">
									<Form.Label>Amount</Form.Label>
									<Form.Control type="number" value={amount} onChange={e => setAmount(e.target.value)} required>
									</Form.Control>
									
								</Form.Group>
								<Form.Group controlId="type2">
									<Form.Label>Type</Form.Label>

									<Form.Control as="select" onChange={e => setType(e.target.value)} required>
										<option>Select type</option>
										<option value="income" >Income</option>
			      						<option value="expense" >Expense</option>
			      					</Form.Control>
								</Form.Group>

								<Form.Group controlId="category2">
									<Form.Label>Category</Form.Label>
									<Form.Control type="text" value={category2} onChange={e => setCategory(e.target.value)} required>
									</Form.Control>
									
								</Form.Group>
								<Button variant="primary" type="submit"id="sumbitBtn">Add Category</Button>						
							</Form>
							</Modal.Body>

					</Modal>*/}	
					<Button name="id" type="button" value={action._id} onClick={e => deleteTransaction(e)}>
						Delete
					</Button>
				</td>
			</tr>

  	

		)
 	
 		
	})
	
 	return (
		<React.Fragment>

			<Form onSubmit={e => createTransaction(e)}>
				<Form.Group controlId="searchbar">
					<Form.Label>Search</Form.Label>
					<Form.Control type="text" placeholder="Search" value={search} onChange={e => setSearch(e.target.value)}/>
					<Form.Text className="text-muted">
					</Form.Text>
				</Form.Group>
				<Button variant="primary" type="submit">
				Submit
				</Button>
			</Form>

			<h4>Transaction History</h4>
			<Table striped bordered hover size="sm">
				<thead>
					<tr>
						<th className="text-center">Description</th>
						<th  className="text-center"colSpan="2" className="text-center">Amount</th>
						<th className="text-center">Type</th>
						<th className="text-center">Category</th>
						<th className="text-center">Date Posted</th>
						<th className="text-center">Month Earned/Spent</th>
						<th className="text-center">Edit/Delete</th>
					</tr>
				</thead>
				<tbody>
					{transactionsData}
					
				</tbody>
			</Table>
			<h4>Filtered Transactions</h4>
			<Table striped bordered hover size="sm" className="mt-3">
				<thead>
					<tr>
						<th>Description</th>
						<th colSpan="2">Amount</th>
						<th>Type</th>
						<th>Category</th>
						<th>Date Posted</th>

					</tr>
				</thead>
				<tbody>
				 	{render}
				</tbody>
			</Table>
		</React.Fragment>
	)
}
