import React, {useState, useContext, useEffect} from 'react';
import {Form, Button, Container, Modal, Row, Col, Badge} from 'react-bootstrap';
import UserContext from '../UserContext'
import Head from 'next/head';
import Swal from 'sweetalert2';
import AppHelper from '../app-helper';
import styles from '../styles/CreateTransactions.module.css'


export default function CreateTransaction () {
//get local user id
	const { user } = useContext(UserContext)


//modal
	const [show, setShow] = useState(false);

	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);


//fetch user transactions from databse
	const [actions, setActions] = useState([])
	const [actions2, setActions2] = useState([])

	const options = {
		method:"POST",
		headers: { 'Content-Type': 'application/json' },
		body: JSON.stringify({
			userID: user.id
		})
	}

	const options2 = {
		method:"POST",
		headers: { 'Content-Type': 'application/json' },
		body: JSON.stringify({
			userID: user.id
		})
	}

//state save even if refreshed
	useEffect(() => {
		const data = localStorage.getItem("storeT");
		const data2 = localStorage.getItem("storeT2");
		// const data2 = localStorage.getItem("storeT");
		if(data) {
		setActions(JSON.parse(data))
		setActions2(JSON.parse(data2))

		}
	},[])


	useEffect( () => {
		fetch(`${ AppHelper.API_URL }/transactions/history`, options)
		.then(res => res.json())
		.then (actions => {
			// console.log(actions)
			// setActions(actions)
			localStorage.setItem("storeT", JSON.stringify(actions))
		})
		fetch(`${ AppHelper.API_URL }/users/getUserCategory`, options2)
		.then(res => res.json())
		.then (actions2 => {
			// console.log(actions2)
			// setActions(actions)
			localStorage.setItem("storeT2", JSON.stringify(actions2))
		})
	})


// console.log(actions)
// console.log(actions2)

//form requirements	
	const [description, setDesc] = useState("")
	const [amount, setAmount] = useState("")
	const [type, setType] = useState("")
	const [category, setCategory] = useState("")
	const [dateEarnedOrSpenT, setdateEarnedOrSpenT] =useState("")

//filtered form
	const [type2, setType2] = useState("")
	const [category2, setCategory2] = useState("")

//conditional render categories according to type
	
	
	// console.log(balance)
	// user transactions mapping
    
    //calculate income
	const filteredIncome = actions.filter(action => action.type == "income")
	const income = filteredIncome.reduce((total, currentValue) => total = total + currentValue.amount,0);

	//calculate expense
	const filteredExpense = actions.filter(action => action.type == "expense")
	const expenses = filteredExpense.reduce((total, currentValue) => total = total + currentValue.amount,0);

	//calculate balance
	const balance = income - expenses

	// console.log(income)
	// console.log(expenses)
	// console.log(balance)
    const filteredActions = actions.filter(action => action.type == type)

	const transactionsData = filteredActions.map(action => {
	
		
		return(

		<option key={action._id} value={action.category}>{action.category}</option>
		)
	})

	//new categories mapping
	// console.log(actions)
	// console.log(actions2)
	const filteredNewCategories = actions2.filter(action2 => action2.type == type)
	const newCategoriesData = filteredNewCategories.map(action2 => {
		return(
		<option key={action2._id} value={action2.category}>{action2.category}</option>
		)
	})
//call this function whenever submmitted
	function createTransaction(e) {
		e.preventDefault()
		// setId("")
		// setName("")
		setDesc("")
		setAmount("")
		setType("")
		setCategory("")
		fetch ('https://cp3-backend-talabucon-carvin.herokuapp.com/api/users/append', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				description: description,
				amount: amount,
				type: type,
		// mobileNo: params.mobileNo,
				category: category
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
			if (data === true) {
				Swal.fire ()
			} else {
				Swal.fire ('Oops!', 'Something went wrong. Please try again.', 'error')
			}
		})

	}


	//call this function when adding new category
	function createCategory(e) {
		
		// setId("")
		// setName("")
		setType("")
		setCategory("")
		fetch ('http://localhost:4000/api/users/newCategory', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				type: type2,
				category: category2
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
			if (data === true) {
				Swal.fire ()
			} else {
				Swal.fire ('Oops!', 'Something went wrong. Please try again.', 'error')
			}
		})

	}



	return (
		<React.Fragment>
			
				
				<Row>
					<Col>
						<Badge variant="primary">
							Balance: {balance}
						</Badge>
					</Col>
				</Row>
				<Row>
					<Col>
						<Badge variant="info">
							Income:{income}
						</Badge>
					</Col>
					<Col>
						<Badge pill variant="warning">
					Expenses:{expenses}
						</Badge>
					</Col>
				</Row>
				<Form className="my-3" onSubmit={e => createTransaction(e)}  className={styles.qwe}>
					<Form.Group controlId="desc">
						<Form.Label>Description</Form.Label>
						<Form.Control as="textarea" placeholder="Enter Transaction Description" value={description} onChange={e => setDesc(e.target.value)} required/>
					</Form.Group>

					<Form.Group controlId="amount">
						<Form.Label>Amount</Form.Label>
						<Form.Control type="number" placeholder="0" value={amount} onChange={e => setAmount(e.target.value)} required/>
					</Form.Group>
					<Form.Group controlId="category">
						<Form.Label>Category</Form.Label>
						<Form.Control as="select" onChange={e => setCategory(e.target.value)} required>
							<option>Select category</option>
							{transactionsData}
							{newCategoriesData}
						</Form.Control>
					</Form.Group>
					<Button variant="primary" type="button" onClick={handleShow}  id="sumbitBtn">Add a category</Button>	
					<Form.Group controlId="type">
						<Form.Label>Type</Form.Label>
						<Form.Control as="select"onChange={e => setType(e.target.value)} required>
							<option>Select type</option>
							<option value="income" >Income</option>
      						<option value="expense" >Expense</option>
      					</Form.Control>
					</Form.Group>
					<Form.Group controlId="date">
						<Form.Label>Date earned or spent. Date today by default</Form.Label>
						<Form.Control type="date" placeholder="0" value={dateEarnedOrSpenT} onChange={e => setdateEarnedOrSpenT(e.target.value)} required/>
					</Form.Group>

											
					<Button variant="primary" type="submit" id="sumbitBtn">Post</Button>

				</Form>
				<Modal
					size="lg"
			        show={show}
			        onHide={handleClose}
			       
			    >
			    	<Modal.Header closeButton>
			    	    <Modal.Title id="example-modal-sizes-title-lg">
							Add Category
						</Modal.Title>
			    	</Modal.Header>
					<Modal.Body>
						<Form className="my-3" onSubmit={e => createCategory(e)}>
							<Form.Group controlId="type2">
								<Form.Label>Type</Form.Label>
								<Form.Control as="select" onChange={e => setType2(e.target.value)} required>
									<option>Select type</option>
									<option value="income" >Income</option>
		      						<option value="expense" >Expense</option>
		      					</Form.Control>
							</Form.Group>

							<Form.Group controlId="category2">
								<Form.Label>Add Category</Form.Label>
								<Form.Control type="text" value={category2} onChange={e => setCategory2(e.target.value)} required>
								</Form.Control>
								
							</Form.Group>
							<Button variant="primary" type="submit"id="sumbitBtn">Add Category</Button>						
						</Form>
					</Modal.Body>
				</Modal>
				
			

		</React.Fragment>
	)

}