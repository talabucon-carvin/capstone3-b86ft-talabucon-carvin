import React from 'react';

// create a Context object
const TokenContext = React.createContext();

//Provider component that allows consuming components to reflect context changes
export constTokenProvider = TokenContext.Provider

export default TokenContext;